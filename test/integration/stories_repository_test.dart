import 'package:flutter_test/flutter_test.dart';
import 'package:news_api/src/data/constants/api_constants.dart';
import 'package:news_api/src/data/model/story_data.dart';
import 'package:news_api/src/domain/api/stories_repository.dart';
import 'package:news_api/src/domain/api/stories_repository_hacker_news_impl.dart';

import '../data/test_data.dart';

void main() {
  group('StoriesRepository', () {
    late StoriesRepository storiesRepository;

    const int batchSize = APIConstants.batchSize;

    setUp(() {
      storiesRepository = StoriesRepositoryHackerNewsImpl();
    });

    test('getStoryById', () async {
      StoryData expected = testStoriesData[0];
      StoryData? nullableStoryData =
          await storiesRepository.getStoryById(expected.id);

      expect(nullableStoryData, isNotNull);

      StoryData storyData = nullableStoryData!;

      expect(storyData.id, equals(expected.id));
      expect(storyData.description, equals(expected.description));
    });

    test('fetchNextStoriesBatch', () async {
      final List<StoryData> stories =
          await storiesRepository.fetchNextStoriesBatch();

      expect(stories.length, equals(batchSize));
    });
  });
}
