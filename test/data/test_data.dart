import 'package:news_api/src/data/model/story_data.dart';

List<StoryData> testStoriesData = [
  StoryData(
    id: "1000000",
    title: "Feral humans are people too",
    description:
        "Feral humans are people too.<p>Can you explain to me why only rich people ought to live in cities?",
    rating: 8,
    timePublished: DateTime.fromMicrosecondsSinceEpoch(1261006251),
  ),
];
