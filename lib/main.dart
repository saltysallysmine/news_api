import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:news_api/src/core/theme_provider.dart';
import 'package:news_api/src/ui/home_page/home_page.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

void main() {
  if (Platform.isWindows || Platform.isLinux) {
    sqfliteFfiInit();
  }
  databaseFactory = databaseFactoryFfi;

  runApp(const ProviderScope(child: App()));
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  bool isDarkModeEnabled = false;

  void toggleTheme() {
    setState(() {
      isDarkModeEnabled = !isDarkModeEnabled;
    });
  }

  Future<bool> requestStoragePermissions() async {
    PermissionStatus status = await Permission.storage.request();
    if (status.isGranted) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: requestStoragePermissions(),
        builder: (context, AsyncSnapshot<void> snapshot) {
          return ThemeProvider(
              isDarkModeEnabled: isDarkModeEnabled,
              toggleTheme: toggleTheme,
              child: MaterialApp(
                title: 'Hacker News API',
                theme: ThemeData(
                  useMaterial3: true,
                  colorScheme: isDarkModeEnabled
                      ? const ColorScheme.dark()
                      : ColorScheme.fromSeed(seedColor: Colors.deepPurple),
                ),
                home: const HomePage(title: 'Best stories'),
              ));
        });
  }
}
