class APIConstants {
  static const String bestStoriesURL =
      'https://hacker-news.firebaseio.com/v0/beststories.json';
  static const batchSize = 5;

  static String getStoryByIdUrl(String storyId) {
    return 'https://hacker-news.firebaseio.com/v0/item/$storyId.json';
  }
}
