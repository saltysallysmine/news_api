class StoryData {
  const StoryData({
    required this.id,
    required this.title,
    required this.description,
    required this.rating,
    required this.timePublished,
    this.url,
  });

  final String id;
  final String title;
  final String description;
  final int rating;
  final DateTime timePublished;
  final String? url;
}
