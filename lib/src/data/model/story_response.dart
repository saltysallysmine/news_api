class StoryResponse {
  // The item's unique id.
  String id;

  // true if the item is deleted.
  String? deleted;

  // The type of item. One of "job", "story", "comment", "poll", or "pollopt".
  String? type;

  // The username of the item's author.
  String? author;

  // Creation date of the item, in Unix Time.

  String? time;

  // The comment, story or poll text. HTML.
  String? text;

  // true if the item is dead.
  String? dead;

  // The comment's parent: either another comment or the relevant story.
  String? parent;

  // The pollopt's associated poll.
  String? poll;

  // The ids of the item's comments, in ranked display order.
  String? kids;

  // The URL of the story.
  String? url;

  // The story's score, or the votes for a pollopt.
  String? score;

  // The title of the story, poll or job. HTML.
  String? title;

  // A list of related pollopts, in display order.
  String? parts;

  // In the case of stories or polls, the total comment count.
  String? descendants;

  StoryResponse({
    required this.id,
    this.deleted,
    this.type,
    this.author,
    this.time,
    this.text,
    this.dead,
    this.parent,
    this.poll,
    this.kids,
    this.url,
    this.score,
    this.title,
    this.parts,
    this.descendants,
  });

  factory StoryResponse.fromJson(Map<String, dynamic> json) {
    StoryResponse result = StoryResponse(
      id: json['id'].toString(),
      deleted: json['deleted'].toString(),
      type: json['type'].toString(),
      author: json['by'].toString(),
      time: json['time'].toString(),
      text: json['text'].toString(),
      dead: json['dead'].toString(),
      parent: json['parent'].toString(),
      poll: json['poll'].toString(),
      kids: json['kids'].toString(),
      url: json['url'].toString(),
      score: json['score'].toString(),
      title: json['title'].toString(),
      parts: json['parts'].toString(),
      descendants: json['descendants'].toString(),
    );
    if (json['text'] == null) {
      result.text = null;
    }
    return result;
  }
}
