class BestStoriesResponse {
  final List<String> storiesIdsList;

  BestStoriesResponse({required this.storiesIdsList});

  factory BestStoriesResponse.fromJson(List<dynamic> json) {
    return BestStoriesResponse(
        storiesIdsList: [for (var element in json) element.toString()]);
  }
}
