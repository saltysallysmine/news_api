import 'package:news_api/src/data/model/story_data.dart';

class SavedNewsDBRecord {
  final int? dbId;
  final String storyId;
  final StoryData storyData;

  const SavedNewsDBRecord(
      {required this.storyId, required this.storyData, this.dbId});

  Map<String, dynamic> toMap() {
    return {
      'story_id': storyData.id,
      'title': storyData.title,
      'description': storyData.description,
      'rating': storyData.rating,
      'time_published': storyData.timePublished.toString(),
      'url': storyData.url,
    };
  }
}
