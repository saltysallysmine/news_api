import 'dart:io';

import 'package:news_api/src/data/model/saved_news_db_record.dart';
import 'package:news_api/src/data/model/story_data.dart';
import 'package:news_api/src/domain/db/saved_news_repository.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SavedStoriesRepositorySQLImpl implements SavedStoriesRepository {
  @override
  Future<Database> openDB() async {
    Directory docsDir = await getApplicationDocumentsDirectory();
    final dbFolderPath = path.join(docsDir.path, 'news_api_data');
    final dbPath = path.join(dbFolderPath, 'saved_news.db');

    Directory directory = Directory(dbFolderPath);
    await directory.create(recursive: true);

    return openDatabase(
      dbPath,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute(
          'CREATE TABLE saved_news (id INTEGER PRIMARY KEY, story_id TEXT,'
          'title TEXT, description TEXT, rating INTEGER, time_published TEXT,'
          'url TEXT)',
        );
      },
    );
  }

  @override
  Future<void> insertSavedNews(StoryData storyData, Database db) async {
    SavedNewsDBRecord savedNews =
        SavedNewsDBRecord(storyId: storyData.id, storyData: storyData);

    await db.insert(
      'saved_news',
      savedNews.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  @override
  Future<void> deleteSavedNewsByStoryId(String storyId, Database db) async {
    await db.delete('saved_news', where: 'story_id = ?', whereArgs: [storyId]);
  }

  @override
  Future<List<StoryData>> getSavedNews(Database db) async {
    final List<Map<String, dynamic>> maps = await db.query(
      'saved_news',
    );

    return List.generate(maps.length, (i) {
      return StoryData(
        id: maps[i]['story_id'],
        title: maps[i]['title'],
        description: maps[i]['description'],
        rating: maps[i]['rating'],
        timePublished: DateTime.parse(maps[i]['time_published']),
        url: maps[i]['url'],
      );
    });
  }

  @override
  Future<StoryData?> findOneByStoryId(String storyId, Database db) async {
    List<Map<String, dynamic>> results = await db.query('saved_news',
        where: 'story_id = ?', whereArgs: [storyId], limit: 1);

    if (results.isNotEmpty) {
      Map<String, dynamic> result = results.first;

      return StoryData(
        id: result['story_id'],
        title: result['title'],
        description: result['description'],
        rating: result['rating'],
        timePublished: DateTime.parse(result['time_published']),
        url: result['url'],
      );
    } else {
      return null;
    }
  }
}
