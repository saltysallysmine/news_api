import 'package:sqflite/sqflite.dart';

import 'package:news_api/src/data/model/story_data.dart';

abstract interface class SavedStoriesRepository {
  Future<Database> openDB();

  Future<void> insertSavedNews(StoryData storyData, Database db);

  Future<void> deleteSavedNewsByStoryId(String storyId, Database db);

  Future<List<StoryData>> getSavedNews(Database db);

  Future<StoryData?> findOneByStoryId(String storyId, Database db);
}
