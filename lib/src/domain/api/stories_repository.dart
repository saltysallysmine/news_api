import 'package:news_api/src/data/model/story_data.dart';

abstract class StoriesRepository {
  Future<StoryData?> getStoryById(String storyId);

  Future<List<StoryData>> fetchNextStoriesBatch();

  List<StoryData> getCachedStoriesData();
}
