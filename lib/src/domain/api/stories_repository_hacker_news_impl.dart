import 'package:dio/dio.dart';
import 'package:news_api/src/data/constants/api_constants.dart';
import 'package:news_api/src/data/model/best_stories_response.dart';
import 'package:news_api/src/data/model/story_response.dart';

import 'package:news_api/src/data/model/story_data.dart';
import 'package:news_api/src/domain/api/stories_repository.dart';

class StoriesRepositoryHackerNewsImpl implements StoriesRepository {
  final String _bestStoriesURL = APIConstants.bestStoriesURL;
  final int _batchSize = APIConstants.batchSize;

  List<String> _bestStoriesIds = [];
  int _returnedBestStoriesCount = 0;
  final List<StoryData> _returnedBestStories = [];

  DateTime _getDateTimeFromUnixTimestamp(String timestamp) {
    int milliseconds = int.parse(timestamp);
    return DateTime.fromMillisecondsSinceEpoch(milliseconds * 1000,
        isUtc: true);
  }

  Future<void> _getBestStoriesList() async {
    Dio dio = Dio();
    Response response = await dio.get(_bestStoriesURL);

    if (response.statusCode != 200) {
      return;
    }

    BestStoriesResponse bestStoriesResponse =
        BestStoriesResponse.fromJson(response.data);

    _bestStoriesIds = bestStoriesResponse.storiesIdsList;
  }

  @override
  Future<StoryData?> getStoryById(String storyId) async {
    Dio dio = Dio();
    Response response = await dio.get(APIConstants.getStoryByIdUrl(storyId));

    if (response.statusCode != 200) {
      return null;
    }

    StoryResponse storyResponse = StoryResponse.fromJson(response.data);

    return StoryData(
        id: storyResponse.id,
        title: storyResponse.title ?? 'No title',
        description: storyResponse.text ?? 'No description',
        rating:
            storyResponse.score == "null" ? 0 : int.parse(storyResponse.score!),
        timePublished: storyResponse.time == null
            ? DateTime.now()
            : _getDateTimeFromUnixTimestamp(storyResponse.time!),
        url: storyResponse.url ?? 'No source link');
  }

  @override
  Future<List<StoryData>> fetchNextStoriesBatch() async {
    if (_returnedBestStoriesCount == 0) {
      await _getBestStoriesList();
    }

    if (_returnedBestStoriesCount >= _bestStoriesIds.length) {
      return [];
    }

    List<StoryData> result = [];

    for (int i = _returnedBestStoriesCount;
        i < _returnedBestStoriesCount + _batchSize;
        ++i) {
      String id = _bestStoriesIds[i];
      StoryData? story = await getStoryById(id);
      if (story != null) {
        result.add(story);
      }
    }

    _returnedBestStoriesCount += _batchSize;
    _returnedBestStories.addAll(result);

    return result;
  }

  @override
  List<StoryData> getCachedStoriesData() {
    return _returnedBestStories;
  }
}
