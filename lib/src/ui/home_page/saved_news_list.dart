import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:news_api/src/core/repositories_providers.dart';
import 'package:news_api/src/data/model/story_data.dart';
import 'package:news_api/src/domain/db/saved_news_repository.dart';
import 'package:sqflite/sqflite.dart';

import 'news_widget.dart';

class SavedNewsList extends StatelessWidget {
  const SavedNewsList({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, _) => _SavedNewsListView(
        savedStoriesRepository: ref.watch(savedNewsRepositoryProvider),
      ),
    );
  }
}

class _SavedNewsListView extends StatefulWidget {
  final SavedStoriesRepository savedStoriesRepository;

  const _SavedNewsListView({required this.savedStoriesRepository});

  @override
  State<_SavedNewsListView> createState() => _SavedNewsListViewState();
}

class _SavedNewsListViewState extends State<_SavedNewsListView> {
  late final List<NewsWidget> _newsList = [];

  Future<void> fetchSavedStories() async {
    Database db = await widget.savedStoriesRepository.openDB();
    for (StoryData storyData
        in await widget.savedStoriesRepository.getSavedNews(db)) {
      _newsList.add(NewsWidget(storyData: storyData));
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<void>(
      future: fetchSavedStories(),
      builder: (context, AsyncSnapshot<void> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (_newsList.isEmpty) {
            return const Center(
              child: Text('You haven\'t got any saved stories right now'),
            );
          }

          return ListView(
            key: UniqueKey(),
            children: _newsList,
          );
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }
}
