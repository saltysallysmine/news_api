import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:news_api/src/core/api_repository_provider.dart';

import 'package:news_api/src/data/model/story_data.dart';
import 'package:news_api/src/domain/api/stories_repository.dart';
import 'package:news_api/src/ui/common_widgets/list_with_header.dart';
import 'news_widget.dart';

void fetchNextStoriesBatchAndCreateNews(
    StreamController<List<NewsWidget>> newsStreamController,
    StoriesRepository storiesRepository) async {
  for (int i = 0; i < 4; ++i) {
    List<NewsWidget> newsBatch = [
      for (StoryData storyData
          in await storiesRepository.fetchNextStoriesBatch())
        NewsWidget(storyData: storyData)
    ];
    newsStreamController.add(newsBatch);
  }
}

// class

class BestNewsList extends StatelessWidget {
  const BestNewsList({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, _) => _BestNewsListView(
        storiesRepository: ref.watch(apiStoriesRepositoryProvider),
      ),
    );
  }
}

class _BestNewsListView extends StatefulWidget {
  final StoriesRepository storiesRepository;

  const _BestNewsListView({required this.storiesRepository});

  @override
  State<_BestNewsListView> createState() => _BestNewsListViewState();
}

class _BestNewsListViewState extends State<_BestNewsListView> {
  late StreamController<List<NewsWidget>> _newsStreamController;
  late final List<NewsWidget> _newsList = [];

  @override
  void initState() {
    super.initState();

    // get cached data
    for (StoryData storyData
        in widget.storiesRepository.getCachedStoriesData()) {
      _newsList.add(NewsWidget(storyData: storyData));
    }

    _newsStreamController = StreamController<List<NewsWidget>>();
    fetchNextStoriesBatchAndCreateNews(
        _newsStreamController, widget.storiesRepository);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<NewsWidget>>(
      stream: _newsStreamController.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          _newsList.addAll(snapshot.data!);
          return ListWithHeader(
            key: UniqueKey(),
            news: _newsList,
          );
        } else if (_newsList.isNotEmpty) {
          return ListWithHeader(
            key: UniqueKey(),
            news: _newsList,
          );
        }

        return const CircularProgressIndicator();
      },
    );
  }

  @override
  void dispose() {
    _newsStreamController.close();
    super.dispose();
  }
}
