import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:news_api/src/ui/detailed_info_page/detailed_info_page.dart';
import 'package:news_api/src/data/model/story_data.dart';

class _Title extends StatelessWidget {
  final String title;

  const _Title({required this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
      overflow: TextOverflow.ellipsis,
    );
  }
}

class _Description extends StatelessWidget {
  final String content;

  const _Description({required this.content});

  @override
  Widget build(BuildContext context) {
    return Text(
      content,
      style: const TextStyle(fontSize: 16),
      overflow: TextOverflow.ellipsis,
    );
  }
}

class _Metadata extends StatelessWidget {
  final int rating;
  final DateTime timePublished;

  const _Metadata({required this.rating, required this.timePublished});

  @override
  Widget build(BuildContext context) {
    String formattedTimePublished =
        DateFormat('yyyy-MM-dd').format(timePublished).toString();

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Rating: $rating',
          style: const TextStyle(fontSize: 14, color: Colors.grey),
        ),
        Text(
          'Published on $formattedTimePublished',
          style: const TextStyle(fontSize: 14, color: Colors.grey),
        ),
      ],
    );
  }
}

class _NewsWidgetCard extends StatelessWidget {
  final StoryData storyData;

  const _NewsWidgetCard({
    required this.storyData,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      margin: const EdgeInsets.all(8),
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _Title(title: storyData.title),
            const SizedBox(height: 8),
            _Description(content: storyData.description),
            const SizedBox(height: 8),
            _Metadata(
                rating: storyData.rating,
                timePublished: storyData.timePublished),
          ],
        ),
      ),
    );
  }
}

class NewsWidget extends StatelessWidget {
  final StoryData storyData;

  const NewsWidget({
    super.key,
    required this.storyData,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => NewsDetailScreen(
              storyData: storyData,
            ),
          ),
        );
      },
      child: Hero(
        tag: 'news_widget:${storyData.id}',
        child: _NewsWidgetCard(
          storyData: storyData,
        ),
      ),
    );
  }
}
