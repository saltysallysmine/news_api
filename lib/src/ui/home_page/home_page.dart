import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:news_api/src/ui/common_widgets/app_bar.dart';
import 'package:news_api/src/ui/common_widgets/bouncing_icon.dart';
import 'package:news_api/src/ui/home_page/best_news_list.dart';
import 'package:news_api/src/ui/home_page/saved_news_list.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentScreen = 0;

  final List<Widget> _screens = [
    const BestNewsList(),
    const SavedNewsList(),
  ];

  void _onTap(int newIndex) {
    setState(() {
      _currentScreen = newIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWithThemeButton(title: widget.title),
      body: Center(
        child: _screens[_currentScreen],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentScreen,
        onTap: _onTap,
        items: [
          BottomNavigationBarItem(
            icon: BouncingIcon(
              key: UniqueKey(),
              icon: Icons.trending_up,
            ),
            label: 'Best stories',
          ),
          BottomNavigationBarItem(
            icon: BouncingIcon(
              key: UniqueKey(),
              icon: Icons.bookmark,
            ),
            label: 'Saved News',
          ),
        ],
      ),
    );
  }
}
