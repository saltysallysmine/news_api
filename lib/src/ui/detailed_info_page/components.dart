part of 'detailed_info_page.dart';

class _Title extends StatelessWidget {
  final String title;

  const _Title({required this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
      overflow: TextOverflow.ellipsis,
    );
  }
}

class _Description extends StatelessWidget {
  final String content;

  const _Description({required this.content});

  @override
  Widget build(BuildContext context) {
    return Text(
      content,
      style: const TextStyle(fontSize: 16),
      overflow: TextOverflow.ellipsis,
    );
  }
}

class _Metadata extends StatelessWidget {
  final int rating;
  final DateTime timePublished;

  const _Metadata({required this.rating, required this.timePublished});

  @override
  Widget build(BuildContext context) {
    String formattedTimePublished =
        DateFormat('yyyy-MM-dd').format(timePublished).toString();

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Rating: $rating',
          style: const TextStyle(fontSize: 14, color: Colors.grey),
          overflow: TextOverflow.ellipsis,
        ),
        Text(
          'Published on $formattedTimePublished',
          style: const TextStyle(fontSize: 14, color: Colors.grey),
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }
}

class _URLButton extends StatelessWidget {
  final String? url;

  const _URLButton({required this.url});

  void _launchURL(String url) async {
    Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        if (url != null) {
          _launchURL(url!);
        }
      },
      child: Text(url == null ? 'No source link' : 'Read full article'),
    );
  }
}

class _SaveNewsButton extends StatelessWidget {
  final bool wereSavedBefore;
  final Function toggleSavedState;

  const _SaveNewsButton(
      {required this.wereSavedBefore, required this.toggleSavedState});

  @override
  Widget build(BuildContext context) {
    return SaveInfoProvider(
        isSaved: wereSavedBefore,
        toggleIsSaved: toggleSavedState,
        child: _SaveNewsButtonView());
  }
}

class _SaveNewsButtonView extends StatefulWidget {
  @override
  State<_SaveNewsButtonView> createState() => _SaveNewsButtonViewState();
}

class _SaveNewsButtonViewState extends State<_SaveNewsButtonView> {
  @override
  Widget build(BuildContext context) {
    SaveInfoProvider savingInfoProvider = SaveInfoProvider.of(context)!;
    return IconButton(
        onPressed: () => savingInfoProvider.toggleIsSaved(),
        icon: savingInfoProvider.isSaved
            ? const Icon(Icons.remove)
            : const Icon(Icons.add));
  }
}
