import 'package:flutter/material.dart';

class SaveInfoProvider extends InheritedWidget {
  final bool isSaved;
  final Function toggleIsSaved;

  const SaveInfoProvider({
    super.key,
    required this.isSaved,
    required this.toggleIsSaved,
    required super.child,
  });

  static SaveInfoProvider? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SaveInfoProvider>();
  }

  @override
  bool updateShouldNotify(SaveInfoProvider oldWidget) {
    return isSaved != oldWidget.isSaved;
  }
}
