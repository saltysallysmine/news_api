import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:news_api/src/core/repositories_providers.dart';
import 'package:news_api/src/data/model/story_data.dart';
import 'package:news_api/src/domain/db/saved_news_repository.dart';
import 'package:news_api/src/ui/common_widgets/app_bar.dart';
import 'package:news_api/src/ui/detailed_info_page/save_info_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:url_launcher/url_launcher.dart';

part 'components.dart';

class NewsDetailScreen extends StatelessWidget {
  final StoryData storyData;

  const NewsDetailScreen({super.key, required this.storyData});

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, ref, _) => Hero(
        tag: 'news_widget:${storyData.id}',
        child: _NewsDetailScreenView(
          storyData: storyData,
          savedStoriesRepository: ref.watch(savedNewsRepositoryProvider),
        ),
        flightShuttleBuilder: (
          BuildContext flightContext,
          Animation<double> animation,
          HeroFlightDirection flightDirection,
          BuildContext fromHeroContext,
          BuildContext toHeroContext,
        ) {
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1.0, 0.0), // Start from the right
              end: const Offset(0.0, 0.0), // End at the original position
            ).animate(animation),
            child: Scaffold(
              appBar: AppBarWithThemeButton(title: storyData.title),
              body: const Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        },
      ),
    );
  }
}

class _NewsDetailScreenView extends StatefulWidget {
  final SavedStoriesRepository savedStoriesRepository;

  final StoryData storyData;

  const _NewsDetailScreenView(
      {required this.storyData, required this.savedStoriesRepository});

  @override
  State<_NewsDetailScreenView> createState() => _NewsDetailScreenViewState();
}

class _NewsDetailScreenViewState extends State<_NewsDetailScreenView> {
  bool isSaved = false;

  void toggleSavedState() async {
    Database db = await widget.savedStoriesRepository.openDB();

    if (!isSaved) {
      await widget.savedStoriesRepository.insertSavedNews(widget.storyData, db);
    } else {
      await widget.savedStoriesRepository
          .deleteSavedNewsByStoryId(widget.storyData.id, db);
    }

    setState(() {
      isSaved = !isSaved;
    });
  }

  Future<void> getSavedStateFromRepo() async {
    Database db = await widget.savedStoriesRepository.openDB();
    StoryData? result = await widget.savedStoriesRepository
        .findOneByStoryId(widget.storyData.id, db);

    isSaved = result == null ? false : true;
  }

  @override
  Widget build(BuildContext context) {
    final StoryData storyData = widget.storyData;

    return FutureBuilder<void>(
        future: getSavedStateFromRepo(),
        builder: (context, AsyncSnapshot<void> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
              appBar: AppBarWithThemeButton(title: storyData.title),
              body: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _Title(title: storyData.title),
                    const SizedBox(height: 8),
                    _Description(content: storyData.description),
                    const SizedBox(height: 8),
                    _Metadata(
                        rating: storyData.rating,
                        timePublished: storyData.timePublished),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _URLButton(url: storyData.url),
                        _SaveNewsButton(
                          wereSavedBefore: isSaved,
                          toggleSavedState: toggleSavedState,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              appBar: AppBarWithThemeButton(title: storyData.title),
              body: const Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }
}
