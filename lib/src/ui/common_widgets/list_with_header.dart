import 'package:flutter/material.dart';
import 'package:news_api/src/ui/home_page/news_widget.dart';

class ListWithHeader extends StatelessWidget {
  final List<NewsWidget> news;

  const ListWithHeader({super.key, required this.news});
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        const SliverAppBar(
          pinned: true,
          expandedHeight: 100.0,
          flexibleSpace: FlexibleSpaceBar(
            titlePadding: EdgeInsets.zero,
            title: Material(
              child: Center(
                child: Text(
                  'Здесь вы можете увидеть наиболее популярные новости HackerNews',
                ),
              ),
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return news[index];
            },
            childCount: news.length,
          ),
        ),
      ],
    );
  }
}
