import 'package:flutter/material.dart';

import 'package:news_api/src/core/theme_provider.dart';

class AppBarWithThemeButton extends StatefulWidget
    implements PreferredSizeWidget {
  const AppBarWithThemeButton({super.key, required this.title});

  final String title;

  @override
  State<AppBarWithThemeButton> createState() => _AppBarWithThemeButtonState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _AppBarWithThemeButtonState extends State<AppBarWithThemeButton> {
  @override
  Widget build(BuildContext context) {
    final themeProvider = ThemeProvider.of(context)!;

    return AppBar(
      backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      title: Text(widget.title),
      actions: [
        IconButton(
            onPressed: () => themeProvider.toggleTheme(),
            icon: themeProvider.isDarkModeEnabled
                ? const Icon(Icons.mode_night)
                : const Icon(Icons.sunny)),
      ],
    );
  }
}
