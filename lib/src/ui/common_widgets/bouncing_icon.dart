import 'package:flutter/material.dart';

class BouncingIcon extends StatefulWidget {
  final Key? key;
  final IconData? icon;

  const BouncingIcon({this.key, required this.icon});

  @override
  State<BouncingIcon> createState() => _BouncingIconState();
}

class _BouncingIconState extends State<BouncingIcon>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _scaleAnimation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );

    _scaleAnimation = Tween<double>(begin: 1.0, end: 4.0).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.easeInOut,
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          _animationController.forward().then((_) {
            _animationController.reverse();
          });
        },
        child: Hero(
          tag: 'bouncing-icon:${widget.key.toString()}',
          child: ScaleTransition(
            scale: _scaleAnimation,
            child: Icon(widget.icon),
          ),
        ),
      ),
    );
  }
}
