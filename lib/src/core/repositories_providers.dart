import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:news_api/src/domain/db/saved_news_repository_sql_impl.dart';

final savedNewsRepositoryProvider =
    Provider((ref) => SavedStoriesRepositorySQLImpl());
