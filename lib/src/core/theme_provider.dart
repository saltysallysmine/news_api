import 'package:flutter/material.dart';

class ThemeProvider extends InheritedWidget {
  final bool isDarkModeEnabled;
  final Function toggleTheme;

  const ThemeProvider({
    super.key,
    required this.isDarkModeEnabled,
    required this.toggleTheme,
    required super.child,
  });

  static ThemeProvider? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ThemeProvider>();
  }

  @override
  bool updateShouldNotify(ThemeProvider oldWidget) {
    return isDarkModeEnabled != oldWidget.isDarkModeEnabled;
  }
}
