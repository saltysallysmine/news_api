import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:news_api/src/domain/api/stories_repository_hacker_news_impl.dart';

final apiStoriesRepositoryProvider =
    Provider((ref) => StoriesRepositoryHackerNewsImpl());
